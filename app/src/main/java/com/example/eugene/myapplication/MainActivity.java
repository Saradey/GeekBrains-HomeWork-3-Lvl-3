package com.example.eugene.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {


    private static final String FILE_NAME = "i1.jpg";
    private static final String FILE_NAME2 = "i1.png";
    int x;
    private TextView viev_text;
    private TextView viev_text2;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viev_text = findViewById(R.id.am_tv_text);
        viev_text2 = findViewById(R.id.am_tv_text2);
        imageView = findViewById(R.id.image2);

        if (Looper.myLooper() == Looper.getMainLooper()) {
            Log.d("MyTag", "UI thread1");
        }
        //single("Какой то текс");
        //flowable();
        //savePicturesToDirectore();
        loadImageAndConvert();
    }


    //////////////////////////////////////////////////
    private void single(String text2) {
        Disposable disposable = Single.just(text2).subscribeWith(new DisposableSingleObserver<String>() {

            @Override
            protected void onStart() {
                Log.d("MyTag", "onStart");
            }

            @Override
            public void onSuccess(@NonNull String str) {
                viev_text.setText(str);
            }

            @Override
            public void onError(@NonNull Throwable e) {
            }

        });
    }
    //////////////////////////////////////////////////


    //////////////////////////////////////////////////
    private void flowable() {
        //int x
        x = 0;

        Flowable.just(1, 2, 3)
                .subscribeOn(Schedulers.io())
                .subscribe(getS());
    }


    @android.support.annotation.NonNull
    private Subscriber<Integer> getS() {
        return new Subscriber<Integer>() {

            @Override
            public void onSubscribe(Subscription s) {
                s.request(Long.MAX_VALUE);
            }

            @Override
            public void onNext(Integer integer) {
                x += integer;
                viev_text2.setText(String.valueOf(x));

                if (Looper.myLooper() == Looper.getMainLooper()) {
                    Log.d("MyTag", "UI thread2");
                }
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onComplete() {
            }
        };
    }

    //////////////////////////////////////////////////
    //сохраняем картинку что бы было что считывать
    private void savePicturesToDirectore() {

        Disposable disposable = Single.just(FILE_NAME)
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableSingleObserver<String>() {

                    @Override
                    protected void onStart() {
                        Log.d("MyTag", "onStart");
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if (Looper.myLooper() == Looper.getMainLooper()) {
                            Log.d("MyTag", "UI thread3");
                        }
                        File file =
                                new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), FILE_NAME);

                        try {
                            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.i1);
                            FileOutputStream fos = new FileOutputStream(file);
                            bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            fos.close();

                            Log.d("MyTest", "Save");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                });
    }


//////////////////////////////////////////////////

    //загружаем конвертируем и сохраняем
    private void loadImageAndConvert() {

        Flowable.just(FILE_NAME)
                .subscribeOn(Schedulers.io())
                .map(v -> {
                    if (Looper.myLooper() == Looper.getMainLooper()) {
                        Log.d("MyTag", "UI thread 4");
                    }
                    File file =
                            new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), FILE_NAME);

                    File file2 =
                            new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), FILE_NAME2);

                    Bitmap myBitmap = null;

                    try {
                        myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        FileOutputStream fos = new FileOutputStream(file);
                        myBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        file.renameTo(file2);
                    } catch (Exception e) {
                        Log.d("MyTag", "Eror load file");
                    }


                    return myBitmap;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Bitmap>() {

                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        if (Looper.myLooper() == Looper.getMainLooper()) {
                            Log.d("MyTag", "UI thread 5");
                        }
                        imageView.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onError(Throwable t) {
                        //t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }





}
